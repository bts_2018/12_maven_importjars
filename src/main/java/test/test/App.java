package test.test;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	String[] fruits = { "Orange", "Apple", "Blueberry", "Guava", "Apple", "Peach", "Orange", "Strawberry" };
		 
		 boolean contains = ArrayUtils.contains(fruits, "Guava");
		 System.out.println("Contains Guava? Answer = " + contains);

		 int indexOfBlueberry = ArrayUtils.indexOf(fruits, "Blueberry");
		 System.out.println("index of Blueberry = " + indexOfBlueberry);

		 int lastIndexOfOrange = ArrayUtils.lastIndexOf(fruits, "Orange");
		 System.out.println("last index of Orange = " + lastIndexOfOrange);
		 
		 //reverse
		 ArrayUtils.reverse(fruits);
		 for(String fruit : fruits) {
				System.out.println("The value is:" +  fruit);
		 }

    }
}
